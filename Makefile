TARGET = flasher
OBJS = src/crt0.o src/main.o src/flasher.o src/giraffe.o src/pack_exec.o src/gzip.o src/utility.o \
        src/progressbar.o src/vlfutility.o src/pspDecrypt.o src/infinity_kinstaller.o src/vlf_driver.o \
        src/scePower.o src/libinfinityUser.o

INCDIR = include .
CFLAGS = -std=c99 -O2 -G0 -Wall
CXXFLAGS = $(CFLAGS) -fno-builtin -fno-exceptions -fno-rtti
ASFLAGS = $(CFLAGS) -c

LIBDIR = lib
LDFLAGS = -nostdlib -nodefaultlibs
LIBS = -lpspsystemctrl_user -lpspvshbridge -lpsppower -lpspipl_update -lz -lvlfgui -lvlfgu -lvlfutils -lvlflibc

BUILD_PRX = 1
USE_KERNEL_LIBS = 0
USE_PSPSDK_LIBC = 1

EXTRA_TARGETS = EBOOT.PBP
PSP_EBOOT_ICON = assets/icon0.png
PSP_EBOOT_TITLE = 6.61 Infinity Firmware Flasher

PSPSDK=$(shell psp-config --pspsdk-path)
include src/build.mak
