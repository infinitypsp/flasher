/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include <pspsdk.h>
#include <psploadcore.h>

#include <string.h>
#include "pack_exec.h"


int GenerateInit(void *out_prx, u32 *out_size, void *init_enc, u32 init_size, void* systimer_enc, u32 systimer_size, void *carbon_prx, u32 carbon_size)
{
	int i;
	int installed_modules = 0;
	
	/* we need to copy systimer.prx and init.prx into carbon */
	for (i = 0; (i < carbon_size); i++)
	{
		/* check for init.prx */
		if (strcmp("psar0:/flash0/kd/init.prx", (char *)carbon_prx + i) == 0)
		{
			/* got it */
			memcpy(carbon_prx + i, init_enc, init_size);
			installed_modules |= 1;
		}
		
		/* check for init.prx */
		else if (strcmp("psar0:/flash0/kd/systimer.prx", (char *)carbon_prx + i) == 0)
		{
			/* got it */
			memcpy(carbon_prx + i, systimer_enc, systimer_size);
			installed_modules |= 2;
		}
	}
	
	/* check for error */
	if (installed_modules != 3)
	{
		/* error occured */
		return -1;
	}
	
	/* pack the executable */
	int res = pack_executable(out_prx, out_size, carbon_prx, carbon_size, 0xDEADBEEF, 0xDEADBEEF, 1, 0, 0, NULL, 1);
	
	/* check for error */
	if (res < 0)
	{
		/* RAGE */
		return res;
	}
	
	/* backup comp_size */
	memcpy(out_prx + 0x4C, out_prx + 0xB0, 4);
	
	/* copy init.prx sigcheck */
	memcpy(out_prx + 0x80, init_enc + 0x80, 0x150 - 0x80);
	
	/* success */
	return 0;
}

void giraffeFixupProgramHeader(Elf32_Ehdr *ehdr)
{
	/* Point to the program headers */
	Elf32_Phdr *phdr = (Elf32_Phdr *)((u32)ehdr + ehdr->e_phoff);
	
	/* dummy everything but .text */
	ehdr->e_phnum = 1;
	
	/* set this to have topaddr */
	phdr->p_vaddr = 0x88F00000;
	
	/* move the program headers into the .text section */
	memcpy((void *)((u32)ehdr + 0x160 - 0x40), phdr, sizeof(Elf32_Phdr));
	
	/* update the entry point 
	ehdr->e_entry = (0x160 - 0x40 + sizeof(Elf32_Phdr));*/
	
	/* update e_phoff */
	ehdr->e_phoff = (0x160 - 0x40);
}

void injectEncryptedSectionHeader(void *dec_data, void *enc_data)
{
	/* copy the encrypted data (enc_data + 0x80) to the ELF */
	memcpy(dec_data + 0x80 - 0x40, enc_data + 0x80, 0x160 - 0x80);
}

sceModInfo *getModuleInfo(Elf32_Ehdr *ehdr)
{
	int i;
	
	/* Point to the program headers */
	Elf32_Phdr *phdr = (Elf32_Phdr *)((u32)ehdr + ehdr->e_phoff);
	
	/* loop through the PRX program headers */
	for (i = 0; i < ehdr->e_phnum; i++, phdr++)
	{
		/* Check for the type == 1 (Moduleinfo) */
		if (phdr->p_type == 1)
		{
			/* check for kernel mode */
			if (phdr->p_paddr & 0x80000000)
			{
				/* lets point to the moduleinfo */
				sceModInfo *modinfo = (sceModInfo *)((u32)ehdr + (phdr->p_paddr & ~0x80000000));
				
				/* check for non kernel */
				if ((modinfo->modattribute & 0x1E00) != 0x1000)
				{
					/* must be a kernel module! */
					goto must_be_kernel;
				}
				
				/* return the offset */
				return modinfo;
			}
			else
			{
				must_be_kernel:
				/* MUST be kernel mode */
				return NULL;
			}
		}
	}

	/* return error */
	return NULL;
}

int mergeGiraffePRX(void *out_prx, u32 *out_size, void *dec_prx, int dec_prx_size, void *enc_prx, int enc_prx_size)
{
	/* declare our local sce header vars */
	u8 dec_sce_header[64];
	u8 enc_sce_header[64];
	
	/* right, lets start by casting our prxs */
	Elf32_Ehdr *ehdr = (Elf32_Ehdr *)dec_prx;
	PSP_Header *psp_header = (PSP_Header *)enc_prx;
	
	/* Force encrypted KIRK checksum and verification code into our ELF */
	injectEncryptedSectionHeader(dec_prx, enc_prx);
	
	/* lets get the module info */
	sceModInfo *modinfo = getModuleInfo(ehdr);
	
	/* check for error */
	if (modinfo == NULL)
	{
		/* error */
		return -1;
	}
	
	/* Store new info */
	strcpy((char*)modinfo->modname, "Giraffe \\o/");
	
	/* remove all ent data */
	modinfo->ent_top = (void *)-1;
	modinfo->ent_end = (void *)-1;
	modinfo->stub_top = (void *)-1;
	modinfo->stub_end = (void *)-1;
	
	/* lets calculate how much we need to pad to get another 64 aligned address */
	int prx_align_pad = dec_prx_size;
	while (prx_align_pad & 0x3F) prx_align_pad++;
	prx_align_pad -= dec_prx_size;
	
	/* lets create the first SCE header now */
	memset(dec_sce_header, 0, sizeof(dec_sce_header));
	
	/* store default data and link to second sce header */
	((u32 *)dec_sce_header)[0] = 0x4543537E;
	((u32 *)dec_sce_header)[1] = sizeof(dec_sce_header) + dec_prx_size + prx_align_pad;
	
	/* now lets create our dual wielding sce and psp header */
	memset(enc_sce_header, 0, sizeof(enc_sce_header));
	memcpy(enc_sce_header, ehdr, sizeof(Elf32_Ehdr));
	
	/* set defaults */
	((u32 *)enc_sce_header)[0] = 0x4543537E;
	((u32 *)enc_sce_header)[1] = 0x00C0FFEE;
	
	/* This is a mixed ~SCE and ELF header */
	Elf32_Ehdr *sce_ehdr = (Elf32_Ehdr *)enc_sce_header;
	
	/* make the program and section offsets backtrack */
	sce_ehdr->e_phoff = 0 - ((dec_prx_size + prx_align_pad) - ehdr->e_phoff);
	sce_ehdr->e_shoff = 0 - ((dec_prx_size + prx_align_pad) - ehdr->e_shoff);
	
	/* check if modinfo is in the compression size */
	if ((psp_header->modinfo_offset - sizeof(enc_sce_header)) < psp_header->psp_size)
	{	
		/* return error */
		return -2;
	}
	
	/* now, lets calculate how much padding after the module we need */
	int modinfo_padding = ((psp_header->modinfo_offset & ~0x80000000) - sizeof(enc_sce_header)) - psp_header->psp_size;
	
	/* copy everything over */
	memcpy(out_prx, dec_sce_header, sizeof(dec_sce_header));
	memcpy(out_prx + sizeof(dec_sce_header), dec_prx, dec_prx_size);
	memcpy(out_prx + sizeof(dec_sce_header) + dec_prx_size + prx_align_pad, enc_sce_header, sizeof(enc_sce_header));
	memcpy(out_prx + sizeof(dec_sce_header) + dec_prx_size + prx_align_pad + sizeof(enc_sce_header), enc_prx, enc_prx_size);
	memcpy(out_prx + sizeof(dec_sce_header) + dec_prx_size + prx_align_pad + sizeof(enc_sce_header) + enc_prx_size + modinfo_padding, modinfo, sizeof(sceModInfo));
	
	memcpy(out_prx + sizeof(dec_sce_header) + (psp_header->modinfo_offset & ~0x80000000), modinfo, sizeof(sceModInfo));
	
	/* give the outsize */
	*out_size = sizeof(dec_sce_header) + dec_prx_size + prx_align_pad + sizeof(enc_sce_header) + enc_prx_size + modinfo_padding + sizeof(sceModInfo);
	
	/* return success */
	return 0;
}

int GenerateSystimer(void *out_prx, u32 *out_size, void *systimer_enc, u32 systimer_enc_size, void *systimer_dec, u32 systimer_dec_size, void *payload, u32 payload_size)
{
	/* cast our parameters */
	Elf32_Ehdr *ehdr = systimer_dec;
	
	/* fix the program headers */
	giraffeFixupProgramHeader(ehdr);
	
	/* get the phdr */
	Elf32_Phdr *phdr = (Elf32_Phdr *)(systimer_dec + ehdr->e_phoff); 
	
	/* copy payload into position */
	memcpy(systimer_dec + (phdr->p_offset + ehdr->e_entry), payload, payload_size);
	
	/* merge the giraffe PRX */
	return mergeGiraffePRX(out_prx, out_size, systimer_dec, systimer_dec_size, systimer_enc, systimer_enc_size);
}
