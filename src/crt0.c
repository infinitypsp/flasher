/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include "utility.h"

#include <pspsdk.h>
#include <pspkernel.h>
#include <stdio.h>
#include <string.h>

#include <vlf.h>

// modules
#include <modules/iop_prx.h>
#include <modules/intrafont_prx.h>
#include <modules/vlf_prx.h>

extern int app_main(int argc, char *argv[]);

char g_local_path[256];

int start_thread(SceSize args, void *argp)
{
	SceUID mod;
	char *path = (char *)argp;
	int last_trail = -1;
	int i;

	if (path)
	{
		for (i = 0; path[i]; i++)
		{
			if (path[i] == '/')
				last_trail = i;
		}
	}

	if (last_trail >= 0)
		path[last_trail] = 0;

	sceIoChdir(path);
	path[last_trail] = '/';

	WriteFile("iop.prx", iop_prx, size_iop_prx);
	mod = sceKernelLoadModule("iop.prx", 0, NULL);
	mod = sceKernelStartModule(mod, args, argp, NULL, NULL);
	WriteFile("intraFont.prx", intrafont_prx, size_intrafont_prx);
	mod = sceKernelLoadModule("intraFont.prx", 0, NULL);
	mod = sceKernelStartModule(mod, args, argp, NULL, NULL);
	WriteFile("vlf.prx", vlf_prx, size_vlf_prx);
	mod = sceKernelLoadModule("vlf.prx", 0, NULL);
	mod = sceKernelStartModule(mod, args, argp, NULL, NULL);
	
	if (last_trail >= 0)
		path[last_trail] = 0;

	strcpy(g_local_path, path);

	path[last_trail] = '/';

	vlfGuiInit(16000, app_main);
	
	return sceKernelExitDeleteThread(0);
}

int module_start(SceSize args, void *argp)
{
	SceUID thid = sceKernelCreateThread("start_thread", start_thread, 0x10, 0x4000, 0, NULL);
	if (thid < 0)
		return thid;

	sceKernelStartThread(thid, args, argp);
	
	return 0;
}
