/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include "vlfutility.h"

#include <pspkernel.h>
#include <pspgum.h>

#include <vlf.h>

#include <stdio.h>
#include <stdarg.h>

static int DoErrorMessage(void *param)
{
    char *error_msg = (char *)param;
    
    vlfGuiSetBackgroundPlane(0xFF3939AA);   
    vlfGuiSetModelSpeed(1.f/15.f);

    ScePspFMatrix4 matrix;
    ScePspFVector3 scale;
    ScePspFVector3 translate;
    translate.x = 0.f;
    translate.y = -13.5f;
    translate.x = 0.f;
    scale.x = scale.y = scale.z = 8.5f;
    gumLoadIdentity(&matrix);   
    gumScale(&matrix, &scale);
    gumRotateZ(&matrix, 3.14159f/2.f);
    gumTranslate(&matrix, &translate);
    vlfGuiSetModelWorldMatrix(&matrix);
    
    vlfGuiMessageDialog(error_msg, VLF_MD_TYPE_ERROR | VLF_MD_BUTTONS_NONE);
    sceKernelExitGame();
    return VLF_EV_RET_REMOVE_HANDLERS;
}

void ErrorExit(const char *fmt, ...)
{
    va_list list;
    static char msg[256];   

    /* collate args into string */
    va_start(list, fmt);
    vsprintf(msg, fmt, list);
    va_end(list);

    /* print string */
    vlfGuiAddEventHandler(0, -1, DoErrorMessage, msg);
    sceKernelSleepThread();
}
