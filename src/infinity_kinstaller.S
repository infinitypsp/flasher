.set noreorder

#include "pspstub.s"

STUB_START "infinity_kinstaller",0x40090000,0x000F0005
STUB_FUNC  0x97B05960,getBaryon
STUB_FUNC  0x6DFEFBF0,getTrueModel
STUB_FUNC  0xBB2FB04A,mfcInit
STUB_FUNC  0x612099B7,mfcEnd
STUB_FUNC  0xFCF3AE95,mfcInitSection
STUB_FUNC  0xFE519CF7,mfcEndSection
STUB_FUNC  0xEDDB2EF3,mfcAddDirectory
STUB_FUNC  0xD0706EA7,mfcAddFile
STUB_FUNC  0x021F11A0,mfcOpen
STUB_FUNC  0xB2605BA2,mfcOpenSection
STUB_FUNC  0x38236022,mfcGetNextFile
STUB_FUNC  0xF162F8D8,mfcCloseSection
STUB_FUNC  0x608088C1,mfcClose
STUB_FUNC  0x5729443A,mfcLflashFatfmtStartFatfmt
STUB_FUNC  0x2AF8F28E,verifyUpdate
STUB_END
