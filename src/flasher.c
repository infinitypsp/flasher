/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "flasher.h"
#include "giraffe.h"
#include "utility.h"
#include "progressbar.h"
#include "vlfutility.h"

#include <infinity_kinstaller.h>
#include <libinfinity.h>

#include <pspkernel.h>
#include <pspctrl.h>
#include <psppower.h>
#include <pspiplupdate.h>

#include <vlf.h>
#include <pspdecrypt.h>

#include <stdio.h>
#include <string.h>
#include <stdarg.h>

// modules to include
#include <modules/pspdecrypt_prx.h>
#include <modules/infinity_kinstaller_prx.h>
#include <modules/psar_driver_prx.h>

// uncomment to enable writing to flash0 instead of ms0
//#define DISABLE_DESTRUCTIVE

void *malloc64(int size);

const unsigned char g_hybrid_sha1[2][0x14] =
{
    // normal fw
    {

        0x9F, 0x23, 0x1F, 0x89, 0x6B, 0xE2, 0x6E, 0x72, 0x6F, 0x20,
        0x21, 0xA1, 0xEE, 0x9F, 0x7E, 0x91, 0xE2, 0xDE, 0x03, 0x84,
    },

    // go fw (ADD)
    {
        0x78, 0x9D, 0x41, 0xC1, 0xAC, 0xE3, 0x46, 0x3C, 0x86, 0x25,
        0x83, 0x66, 0x49, 0x07, 0x54, 0x19, 0xB3, 0x02, 0x79, 0xC6
    }
};

char *g_psp_phat_exclusion[] =
{
	"flash0:/vsn/module/lftv_main_plugin.prx",
	"flash0:/vsn/module/lftv_middleware.prx",
	"flash0:/vsn/module/lftv_plugin.prx",
	"flash0:/vsn/resource/lftv_main_plugin.rco",
	"flash0:/vsn/resource/lftv_rmc_univer3in1.rco",
	"flash0:/vsn/resource/lftv_rmc_univer3in1_jp.rco",
	"flash0:/vsn/resource/lftv_rmc_univerpanel.rco",
	"flash0:/vsn/resource/lftv_rmc_univerpanel_jp.rco",
	"flash0:/vsn/resource/lftv_rmc_univertuner.rco",
	"flash0:/vsn/resource/lftv_rmc_univertuner_jp.rco",
	"flash0:/vsn/resource/lftv_tuner_jp_jp.rco",
	"flash0:/vsn/resource/lftv_tuner_us_en.rco"
};

#define PSP_PHAT_EXCLUSION_N    (sizeof(g_psp_phat_exclusion)/sizeof(char *))

#define DATA_BUFFER_SIZE        (10*1024*1024)
static u8 *g_databuffer;

int scePowerRequestColdReset(int unk);

extern char g_local_path[];

const char *updatePath(void)
{
    return g_local_path;
}

volatile int g_signal = 0;

int resume(void *param)
{
    vlfGuiSetBackgroundIndex(8);
    g_signal = 1;
    return VLF_EV_RET_REMOVE_HANDLERS;
}

int displayerr(void *param)
{
    char *msg = (char *)param;

    vlfGuiSetBackgroundPlane(0xFF3939AA);
    SetProgressBarStatus(msg);

    // resume on X
    vlfGuiAddEventHandler(PSP_CTRL_ENTER, 0, resume, NULL);
	return VLF_EV_RET_REMOVE_HANDLERS;
}

void notify_error(const char *fmt, ...)
{
	va_list list;
    static char msg[256];

	/* collate args into string */
	va_start(list, fmt);
	vsprintf(msg, fmt, list);
	va_end(list);


	vlfGuiChangeCharacterByButton('*', VLF_ENTER);
    strcat(msg, "\nPress * to continue.");

    g_signal = 0;

	vlfGuiAddEventHandler(0, -1, displayerr, msg);

    while (g_signal == 0)
    {
        sceKernelDelayThread(10000);
    }
}

static void load_kernel_modules(void)
{
    SetProgressBarStatus("Loading kernel modules...");

    // write our kernel helpers
    int res = WriteFile("pspdecrypt.prx", pspdecrypt_prx, size_pspdecrypt_prx);

    if (res != size_pspdecrypt_prx)
    {
    ErrorExit("Error 0x%08X writing pspdecrypt.", res);
    }

    res = WriteFile("infinity_kinstaller.prx", infinity_kinstaller_prx, size_infinity_kinstaller_prx);

    if (res != size_infinity_kinstaller_prx)
    {
    ErrorExit("Error 0x%08X writing infinity_kinstaller.", res);
    }

    res = WriteFile("psar_driver.prx", psar_driver_prx, size_psar_driver_prx);

    if (res != size_psar_driver_prx)
    {
        ErrorExit("Error 0x%08X writing psar_driver.", res);
    }

    // load our kernel helpers
    SceUID modid = LoadStartModule("pspdecrypt.prx");

    if (modid < 0)
    {
        ErrorExit("Error 0x%08X loading pspdecrypt.", modid);
    }

    modid = LoadStartModule("infinity_kinstaller.prx");

    if (modid < 0)
    {
        ErrorExit("Error 0x%08X loading infinity kernel module.", modid);
    }

    modid = LoadStartModule("psar_driver.prx");

    if (modid < 0)
    {
        ErrorExit("Error 0x%08X loading psar_driver.", modid);
    }
}

static void format_flash(const char *dev, const char *blk)
{
    int res;
    char *argv[2];
    argv[0] = "fatfmt";
	argv[1] = blk;

#ifndef DISABLE_DESTRUCTIVE
	if ((res = mfcLflashFatfmtStartFatfmt(2, argv)) < 0)
    {
		ErrorExit("Error 0x%08X in mfcLflashFatfmtStartFatfmt!!", res);
    }
#endif
}

static void load_start_updater_modules(void)
{
	int size, res;
	char filename[256];

    const char *g_updater_modules[] =
    {
        "emc_sm_updater.prx",
        "lfatfs_updater.prx",
        "lflash_fatfmt.prx",
        "ipl_update.prx"
    };

	if (mfcOpenSection("UPD") < 0)
	{
		ErrorExit("Error opening \"%s\" section.", "UPD");
	}

    // read every file
	while (mfcGetNextFile(filename, &size,  NULL, NULL, g_databuffer, DATA_BUFFER_SIZE) >= 0)
	{
		if (WriteFile(filename, g_databuffer, size) != size)
		{
            ErrorExit("Error writing temporary file %s.", filename);
		}
	}

	if (mfcCloseSection() < 0)
	{
		ErrorExit("Error closing \"%s\" section.", "UPD");
	}

    int i;
    for (i = 0; i < sizeof(g_updater_modules)/sizeof(char *); ++i)
    {
        res = LoadStartModule(g_updater_modules[i]);
        if (res < 0)
        {
            ErrorExit("Error 0x%08X loading/starting %s.", res, g_updater_modules[i]);
        }
    }
}

static void create_directories(void)
{
    int res;
    char filename[256];

	if (mfcOpenSection("DIR") < 0)
	{
        // no point exiting it'll be a brick
		notify_error("Error opening section \"%s\"!!", "DIR");
	}

	while (mfcGetNextFile(filename, NULL, NULL, NULL, NULL, 0) >= 0)
	{
#ifdef DISABLE_DESTRUCTIVE
        memcpy(filename, "ms0:/f0/", 8);
#endif

		if ((res = sceIoMkdir(filename, 0777)) < 0)
		{
            // no point exiting it'll be a brick
			notify_error("Error 0x%08X in sceIoMkdir \"%s\"!!", res, filename);
		}
	}

	if ((res = mfcCloseSection()) < 0)
	{
        // no point exiting it'll be a brick
		notify_error("Error 0x%08X closing section \"%s\".", res, "DIR");
	}
}

static int is_excluded(const char *file)
{
    if (getTrueModel() != 0)
    {
        // only PSP 1000 is excluded because of 32 MB NAND
        return 0;
    }

    int i;
    for (i = 0; i < PSP_PHAT_EXCLUSION_N; ++i)
    {
        if (strcmp(g_psp_phat_exclusion[i], file) == 0)
        {
            //  is excluded
            return 1;
        }
    }

    return 0;
}

static void flash_files(const char *section, int start, int end)
{
	int size, cur, total;
	char file[256];

    if (mfcOpenSection((char *)section) < 0)
	{
        // no point exiting it'll be a brick
		notify_error("Error opening section \"%s\".", section);
	}

	while (mfcGetNextFile(file, &size,  &cur, &total, g_databuffer, DATA_BUFFER_SIZE) >= 0)
	{
#ifdef DISABLE_DESTRUCTIVE
        memcpy(file, "ms0:/f0/", 8);
#endif

        // check to see if we are excluding this file
        if (is_excluded(file))
        {
            continue;
        }

        if (WriteFile(file, g_databuffer, size) != size)
        {
            // no point exiting, it'll be a brick regardless
            notify_error("Error writing %s.", file);
        }

        SetProgressBarPercentage(start+(((end-start)*cur)/(total)), 0);
	}

	if (mfcCloseSection() < 0)
	{
		notify_error("Error closing section \"%s\".", section);
	}
}

static void write_ipl(const char *section)
{
	int size, cur, total;
	char file[256];

    if (mfcOpenSection((char *)section) < 0)
	{
        // no point exiting it'll be a brick
		notify_error("Error opening section \"%s\".", section);
	}

	while (mfcGetNextFile(file, &size,  &cur, &total, g_databuffer, DATA_BUFFER_SIZE) >= 0)
	{
#ifndef DISABLE_DESTRUCTIVE
        if (sceIplUpdateClearIpl() < 0)
        {
            // no point exiting, it'll be a brick regardless
            notify_error("Error in sceIplUpdateClearIpl.");
        }

        if (sceIplUpdateSetIpl(g_databuffer, size) < 0)
        {
            // no point exiting, it'll be a brick regardless
            notify_error("Error in sceIplUpdateSetIpl.");
        }
#endif
	}

	if (mfcCloseSection() < 0)
	{
		notify_error("Error closing section \"%s\".", section);
	}
}

static void sign_bootloader(const char *section)
{
	int size, cur, total;
	char file[256];
    u8 *bootloader = NULL, *payload = NULL, *systimer = NULL, *init = NULL;
    int bootloader_size = 0, payload_size = 0, systimer_size = 0 , init_size = 0;

    if (mfcOpenSection((char *)section) < 0)
	{
        // no point exiting it'll be a brick
		notify_error("Error opening section \"%s\".", section);
        return;
	}

	while (mfcGetNextFile(file, &size,  &cur, &total, g_databuffer, DATA_BUFFER_SIZE) >= 0)
	{
        // read in all the files in the bootloader
        if (strcmp(file, "bootloader.prx") == 0)
        {
            bootloader = malloc64(size);

            if (bootloader == NULL)
            {
                notify_error("Error allocating 0x%08X bytes.", size);
                return;
            }

            memcpy(bootloader, g_databuffer, size);
            bootloader_size = size;
        }
        else if (strcmp(file, "bootloader_payload.bin") == 0)
        {
            payload = malloc64(size);

            if (payload == NULL)
            {
                notify_error("Error allocating 0x%08X bytes.", size);
                return;
            }

            memcpy(payload, g_databuffer, size);
            payload_size = size;
        }
        else if (strcmp(file, "systimer.prx") == 0)
        {
            systimer = malloc64(size);

            if (systimer == NULL)
            {
                notify_error("Error allocating 0x%08X bytes.", size);
                return;
            }

            memcpy(systimer, g_databuffer, size);
            systimer_size = size;
        }
        else if (strcmp(file, "init.prx") == 0)
        {
            init = malloc64(size);

            if (init == NULL)
            {
                notify_error("Error allocating 0x%08X bytes.", size);
                return;
            }

            memcpy(init, g_databuffer, size);
            init_size = size;
        }
	}

    // decrypt systimer.prx
    int res = pspDecryptPRX(systimer, g_databuffer, systimer_size);

    if (res < 0)
    {
        notify_error("Error 0x%08X decrypting 6.31 systimer.prx.", res);
    }

    u8 *dec_systimer = g_databuffer + res;
    while ((u32)dec_systimer % 64) dec_systimer++;

    int dec_systimer_size = pspDecompress(g_databuffer, dec_systimer, (u32)g_databuffer+DATA_BUFFER_SIZE-(u32)dec_systimer);

    if (dec_systimer_size < 0)
    {
        notify_error("Error 0x%08X decompressing 6.31 systimer.prx", dec_systimer_size);
        return;
    }

    res = pspSignCheck(systimer);

    if (res < 0)
    {
        notify_error("Error 0x%08X signchecking systimer.prx.", res);
        return;
    }

    u8 *giraffe_systimer = dec_systimer+dec_systimer_size;
    while ((u32)giraffe_systimer % 64) giraffe_systimer++;
    unsigned int giraffe_systimer_size = 0;

    res = GenerateSystimer(giraffe_systimer, &giraffe_systimer_size, systimer, systimer_size, dec_systimer, dec_systimer_size, payload, payload_size);

    if (res < 0)
    {
        notify_error("Error 0x%08X generating giraffe_systimer.prx.", res);
        return;
    }

#ifdef DISABLE_DESTRUCTIVE
    if (WriteFile("ms0:/f0/kd/systimer.prx", giraffe_systimer, giraffe_systimer_size) != giraffe_systimer_size)
#else
    if (WriteFile("flash0:/kd/systimer.prx", giraffe_systimer, giraffe_systimer_size) != giraffe_systimer_size)
#endif
    {
        // no point exiting, it'll be a brick regardless
        notify_error("Error writing bootloader systimer.prx.");
    }

    // signcheck init.prx
    res = pspSignCheck(init);

    if (res < 0)
    {
        notify_error("Error 0x%08X signchecking init.prx.", res);
        return;
    }

    u8 *packed_bootloader = g_databuffer;
    unsigned int packed_bootloader_size = 0;

    // generate our init.prx replacement
    res = GenerateInit(packed_bootloader, &packed_bootloader_size, init, init_size, systimer, systimer_size, bootloader, bootloader_size);

    if (res < 0)
    {
        notify_error("Error 0x%08X generating packed_bootloader.prx.", res);
        return;
    }

#ifdef DISABLE_DESTRUCTIVE
    if (WriteFile("ms0:/f0/kd/init.prx", packed_bootloader, packed_bootloader_size) != packed_bootloader_size)
#else
    if (WriteFile("flash0:/kd/init.prx", packed_bootloader, packed_bootloader_size) != packed_bootloader_size)
#endif
    {
        // no point exiting, it'll be a brick regardless
        notify_error("Error writing bootloader init.prx.");
        return;
    }

	if (mfcCloseSection() < 0)
	{
		notify_error("Error closing section \"%s\".", section);
        return;
	}

    free(bootloader);
    free(payload);
    free(systimer);
    free(init);
}

static int OnInstallComplete(void *param)
{
    vlfGuiSetModelSpeed(1.f/60.f);

    RemoveProgressBar(0);
    SetProgressBarStatus("Complete. Press * to reboot.");

    vlfGuiAddEventHandler(PSP_CTRL_ENTER, 0, (int (*)(void *))scePowerRequestColdReset, NULL);
	return VLF_EV_RET_REMOVE_HANDLERS;
}


int FlashHybridFirmware(void)
{
    // allocate memory
    g_databuffer = malloc64(DATA_BUFFER_SIZE);

    if (g_databuffer == NULL)
    {
        ErrorExit("Not enough memory for internal buffers!");
    }

    // move to UPDATE directory
    sceIoChdir(updatePath());

    SetProgressBarPercentage(0, 1);

    // begin loading kernel modules
    load_kernel_modules();
	sceKernelDelayThread(1200000);
    SetProgressBarPercentage(1, 1);

    // verify hybrid archive
    SetProgressBarStatus("Verifying hybrid firmware...");
    int res = 0;//VerifyFileSHA1("DATA.MFC", g_hybrid_sha1[is_psp_go()], g_databuffer, DATA_BUFFER_SIZE/8);

    if (res < 0)
    {
        ErrorExit("Error 0x%08X verifying hybrid firmware. Please regenerate using the firmware builder.", res);
    }

    SetProgressBarPercentage(5, 1);

    if (mfcOpen("DATA.MFC") < 0)
	{
		ErrorExit("Error opening DATA.MFC.");
	}

    // unassign flash0/1/2/3
	if ((res = sceIoUnassign("flash0:")) < 0)
    {
        WriteFile("ms0:/flash0.bin", &res, 4);
		ErrorExit("Error 0x%08X in sceIoUnassign (0).", res);
    }

	if ((res = sceIoUnassign("flash1:")) < 0)
    {
        WriteFile("ms0:/flash1.bin", &res, 4);
		ErrorExit("Error 0x%08X in sceIoUnassign (1).", res);
    }

    // ignore errors
    sceIoUnassign("flash2:");
	sceIoUnassign("flash3:");

    SetProgressBarStatus("Loading special updater modules...");
    load_start_updater_modules();
	sceKernelDelayThread(1200000);
    SetProgressBarPercentage(6, 1);

#ifndef DISABLE_DESTRUCTIVE
    int isInfinity = !(infGetVersion() & 0x80000000);

    if (isInfinity)
    {
        infSetRedirectionStatus(0);
    }

    // format flash 0
    SetProgressBarStatus("Formatting flash0...");
    format_flash("flash0", "lflash0:0,0");
	sceKernelDelayThread(1200000);
    SetProgressBarPercentage(7, 1);

    // format flash 1
    SetProgressBarStatus("Formatting flash1...");
    format_flash("flash1", "lflash0:0,1");
	sceKernelDelayThread(1200000);
    SetProgressBarPercentage(8, 1);

    // format flash 2
    SetProgressBarStatus("Formatting flash2...");
    format_flash("flash2", "lflash0:0,2");
	sceKernelDelayThread(1200000);
    SetProgressBarPercentage(9, 1);

    // reassign flashes
    SetProgressBarStatus("Assigning flashes...");

	if ((res = sceIoAssign("flash0:", "lflash0:0,0", "flashfat0:", IOASSIGN_RDWR, NULL, 0)) < 0)
		notify_error("Error 0x%08X in sceIoAssign (0).", res);

	if ((res = sceIoAssign("flash1:", "lflash0:0,1", "flashfat1:", IOASSIGN_RDWR, NULL, 0)) < 0)
		notify_error("Error 0x%08X in sceIoAssign (1).", res);

	sceIoAssign("flash2:", "lflash0:0,2", "flashfat2:", IOASSIGN_RDWR, NULL, 0);

	sceKernelDelayThread(1200000);
    SetProgressBarPercentage(10, 1);
#endif

    // create directories
    SetProgressBarStatus("Creating directories...");
    create_directories();
	sceKernelDelayThread(1200000);
    SetProgressBarPercentage(11, 1);

    // write the 6.31 firmware subset
    SetProgressBarStatus("Writing 6.31 subset...");
    flash_files("6.31", 11, 30);
    SetProgressBarPercentage(30, 1);

    // write the 6.61 firmware subset
    SetProgressBarStatus("Writing 6.61 firmware...");
    flash_files("6.61", 30, 95);
    SetProgressBarPercentage(95, 1);

#ifndef DISABLE_DESTRUCTIVE
    SetProgressBarStatus("Writing IPL");
    write_ipl("IPL");
	sceKernelDelayThread(1200000);
    SetProgressBarPercentage(96, 1);
#endif

    SetProgressBarStatus("Signing and writing bootloader...");
    sign_bootloader("BL");
	sceKernelDelayThread(1200000);
    SetProgressBarPercentage(97, 1);

    SetProgressBarStatus("Writing 6.61 firmware controller...");
    flash_files("CFW", 97, 100);
    SetProgressBarPercentage(100, 1);
	sceKernelDelayThread(1200000);

    vlfGuiAddEventHandler(0, 600000, OnInstallComplete, NULL);
    return sceKernelExitDeleteThread(0);
}
