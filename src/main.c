/*

Copyright (C) 2015, David "Davee" Morgan

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.

*/

#include "progressbar.h"
#include "flasher.h"
#include "utility.h"
#include "vlfutility.h"
#include "wave.h"
#include "background.h"

#include <pspkernel.h>
#include <pspgum.h>

#include <vlf.h>

PSP_MODULE_INFO("infinity_maker", 0x0800, 1, 0);
PSP_MAIN_THREAD_ATTR(PSP_THREAD_ATTR_VSH);

static char *g_main_menu[] =
{
    "Start",
};

#define MAIN_MENU_ITEM_N    (sizeof(g_main_menu)/sizeof(char *))

u8 g_bg_bmp[0x1820];

char *g_agreement_text =
		"6.61 Infinity Agreement.\n"
        "This is an imaginary sample license.\n"
		"By using this software and flashing\n"
        "6.61 Infinity, you agree that you take\n"
        "ALL responsibility should your device\n"
        "no longer operate. This software is\n"
        "provided FREE of charge and comes with\n"
        "no warranty and no guarantee.\n"
        "After installation you will be required\n"
        "to input all system settings as the\n"
        "flashing process will wipe them.\n\n"
        "BY ACCEPTING THIS AGREEMENT\n"
        "YOU EXPLICITLY AGREE TO\n"
        "ACCEPT RESPONSIBILITY FOR ANY\n"
        "NEGATIVE CONSEQUENCES.";

VlfText g_agreement, g_agree_text;
int g_begin_wave_rotation = 0;
float g_wave_rotation = 3.14f/2.f;

static void OnInstallAgreementFadeOutComplete(void *param)
{
	// Destruct old items
	vlfGuiRemoveText(g_agreement);
	vlfGuiRemoveText(g_agree_text);
	vlfGuiCancelBottomDialog();

    // begin progress bar
    InitProgressBar(136);

    // force wave to move to angle 0
    g_begin_wave_rotation = 1;

    SceUID thid = sceKernelCreateThread("flasher_thread", (SceKernelThreadEntry)&FlashHybridFirmware, 0x40, 0x10000, PSP_THREAD_ATTR_VSH, NULL);

    if (thid >= 0)
    {
        sceKernelStartThread(thid, 0, NULL);
    }
    else
    {
        ErrorExit("could not create thread!\n");
    }

	vlfGuiSetRectangleFade(0, VLF_TITLEBAR_HEIGHT, 480, 272-VLF_TITLEBAR_HEIGHT, VLF_FADE_MODE_IN, VLF_FADE_SPEED_SUPER_FAST, 0, NULL, NULL, 0);
}

static int OnInstallAgreement(int enter)
{
	if (enter)
	{
		vlfGuiSetRectangleFade(0, VLF_TITLEBAR_HEIGHT, 480, 272-VLF_TITLEBAR_HEIGHT, VLF_FADE_MODE_OUT, VLF_FADE_SPEED_SUPER_FAST, 0, OnInstallAgreementFadeOutComplete, NULL, 0);
		return VLF_EV_RET_NOTHING;
	}

	sceKernelExitGame();
	return VLF_EV_RET_NOTHING;
}

static void SetupAgreement(void)
{
	g_agreement = vlfGuiAddText(100, 40, g_agreement_text);
	vlfGuiSetTextScrollBar(g_agreement,  90);
	g_agree_text = vlfGuiAddText(120, 210, "Do you agree to the terms?");
	vlfGuiBottomDialog(VLF_DI_CANCEL, VLF_DI_ENTER, 1, 0, VLF_DEFAULT, OnInstallAgreement);
	vlfGuiSetRectangleFade(0, VLF_TITLEBAR_HEIGHT, 480, 272-VLF_TITLEBAR_HEIGHT, VLF_FADE_MODE_IN, VLF_FADE_SPEED_SUPER_FAST, 0, NULL, NULL, 0);
}

static int OnStartFlasher(int enter)
{
	if (enter)
	{
        vlfGuiCancelBottomDialog();
        vlfGuiCancelCentralMenu();
        SetupAgreement();
		return VLF_EV_RET_NOTHING;
	}

	sceKernelExitGame();
	return VLF_EV_RET_REMOVE_OBJECTS | VLF_EV_RET_REMOVE_HANDLERS;
}

int app_main(int argc, char *argv[])
{
    // setup VLF
	vlfGuiSystemSetup(1, 1, 1);
    vlfGuiSetModel(g_gmoWave, size_g_gmoWave);
    vlfGuiSetModelSpeed(1.f/45.f);

    // load our background
    vlfGuiSetBackgroundFileBuffer(g_background, size_g_background, 1);

    VlfText title_bar = vlfGuiAddText(0, 0, "6.61 Infinity Firmware Flasher");
    VlfPicture update_icon = vlfGuiAddPictureResource("update_plugin", "tex_update_icon", 0, 0);

	vlfGuiChangeCharacterByButton('*', VLF_ENTER);

    // cache this as flash0 will not be available later
	vlfGuiCacheResource("system_plugin");
	vlfGuiCacheResource("system_plugin_fg");

    // set our title bar
    vlfGuiSetTitleBar(title_bar, update_icon, 1, 0);

    // use a central menu for this
	vlfGuiCentralMenu(MAIN_MENU_ITEM_N, g_main_menu, 0, NULL, 0, 0);
	vlfGuiBottomDialog(VLF_DI_CANCEL, VLF_DI_ENTER, 1, 0, VLF_DEFAULT, OnStartFlasher);

	while (1)
	{
        ScePspFMatrix4 matrix;
        ScePspFVector3 scale;
        ScePspFVector3 translate;
        translate.x = 0.f;
        translate.y = -13.5f;
        translate.x = 0.f;
        scale.x = scale.y = scale.z = 8.5f;
        gumLoadIdentity(&matrix);
        gumScale(&matrix, &scale);
        gumRotateZ(&matrix, g_wave_rotation);
        gumTranslate(&matrix, &translate);
        vlfGuiSetModelWorldMatrix(&matrix);

		vlfGuiDrawFrame();

        if (g_begin_wave_rotation)
        {
            g_wave_rotation -= 0.001f;

            if (g_wave_rotation < 0)
                g_wave_rotation = 0.f;
        }
	}

    return 0;
}
