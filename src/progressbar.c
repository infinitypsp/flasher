/*

Copyright (C) 2015, David "Davee" Morgan 

Permission is hereby granted, free of charge, to any person obtaining a 
copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation 
the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the 
Software is furnished to do so, subject to the following conditions: 

The above copyright notice and this permission notice shall be included in 
all copies or substantial portions of the Software. 

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL 
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING 
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
DEALINGS IN THE SOFTWARE. 

*/

#include "progressbar.h"

#include <pspsdk.h>
#include <vlf.h>
#include <stdio.h>
#include <string.h>

static VlfText g_progress_status;
static VlfText g_progress_percent;
static VlfProgressBar g_progress_bar;
static int g_last_percentage = -1;
static int g_last_time = -1;
static char g_percentage_text[20];
static char g_status_text[128];

static int DoStatusUpdate(void *param)
{
	vlfGuiSetText(g_progress_status, g_status_text);
	return VLF_EV_RET_REMOVE_HANDLERS;
}

static int DoProgressUpdate(void *param)
{
	vlfGuiProgressBarSetProgress(g_progress_bar, g_last_percentage);
	vlfGuiSetText(g_progress_percent, g_percentage_text);
	
	return VLF_EV_RET_REMOVE_HANDLERS;
}

void InitProgressBar(int y)
{
    ResetProgressBar();
    g_progress_status = vlfGuiAddText(80, y-36, "");
    g_progress_bar = vlfGuiAddProgressBar(y);
	g_progress_percent = vlfGuiAddText(240, y+12, "0%");
	vlfGuiSetTextAlignment(g_progress_percent, VLF_ALIGNMENT_CENTER);
}

void SetProgressBarStatus(const char *status)
{
	strcpy(g_status_text, status);
	vlfGuiAddEventHandler(0, -2, DoStatusUpdate, NULL);
}

void SetProgressBarPercentage(int percentage, int force)
{
	int st = sceKernelGetSystemTimeLow();
	
	if (force || (percentage > g_last_percentage && st >= (g_last_time+520000)))
	{		
		sprintf(g_percentage_text, "%d%%", percentage);
		g_last_percentage = percentage;
		g_last_time = st;
		vlfGuiAddEventHandler(0, -2, DoProgressUpdate, NULL);
	}
}

void ResetProgressBar(void)
{
	g_last_time = 0;
	g_last_percentage = -1;
}

void RemoveProgressBar(int incstatus)
{
	vlfGuiRemoveText(g_progress_percent);
	vlfGuiRemoveProgressBar(g_progress_bar);
    
    if (incstatus)
    {
        vlfGuiRemoveText(g_progress_status);
    }
}
